package com.klab;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.ResourceLoader;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import com.ibm.cloud.sdk.core.security.Authenticator;
import com.ibm.cloud.sdk.core.security.IamAuthenticator;
import com.ibm.watson.assistant.v1.Assistant;
import com.ibm.watson.assistant.v1.model.Context;
import com.ibm.watson.assistant.v1.model.MessageInput;
import com.ibm.watson.assistant.v1.model.MessageOptions;
import com.ibm.watson.assistant.v1.model.MessageResponse;
import com.klab.dao.UserDao;
import com.klab.model.User;

@Component
//@SuppressWarnings({ "rawtypes", "unchecked" })
public class WAService
{
	static class UserSession
	{
		public String userId;
		public User userInfo;
		public Context context;
		
		public String toString() {
			return userInfo.getUserNm();
		}
	}
	
    @Value("${wa.api}")
    private String apiKey;

    @Value("${wa.wks}")
    private String workspaceId;

    private Assistant service;
    private Map<String, UserSession> userSession;
    
    @Autowired
    private UserDao userDao;
    
    private ResourceLoader resourceLoader;
    @Autowired
    public WAService(ResourceLoader resourceLoader) {
        this.resourceLoader = resourceLoader;
    }
    
	@PostConstruct
    public void init() {
		Authenticator authenticator = new IamAuthenticator(apiKey);
		service = new Assistant("2019-02-28", authenticator);
		//context = new Context();
		
		userSession = new HashMap<>();
		
//		try {
//			Resource resource = resourceLoader.getResource("classpath:user.json");
//			FileReader fr = new FileReader(resource.getFile());
//			Map map = new HashMap<String, Object>();
//			map = (HashMap<String, Object>)new Gson().fromJson(fr, map.getClass());
//
//			List<Map> users = (List<Map>)map.get("users");
//
//			userSession = new ArrayList<>();
//			
//			for (Map m : users) {
//				UserSession us = new UserSession();
//				
//				String userId = (String)m.get("userId");
//				
//				us.userId = userId;
//				us.userInfo = m;
//				us.context = new Context();
//				
//				userSession.add(us);
//			}
//
//			System.out.println(user.getUserName());
//			
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
		
    }
	
//    /**
//     * @param userId
//     * @return
//     */
//    private UserSession getSession(String userId)
//    {
//    	UserSession rs = userSession.get(userId);
//    	
//    	if ( rs == null ) {
//    		try {
//    			rs = fetchUserInfo(userId);
//    		}catch(Exception e) {}
//    	}
//   
//    	return rs;
//    }
    
    /**
     * @param userId
     * @throws Exception
     */
    private UserSession fetchUserInfo(String userId) throws Exception
    {
		User us = userDao.selectUser(userId);
		
		UserSession sess = new UserSession();
		sess.userId = userId;
		sess.userInfo = us;
		sess.context = new Context();
				
		userSession.put(userId, sess);
		
		return sess;
    }
    
	/**
	 * @param userId
	 * @param text
	 * @param wasSession
	 * @return
	 */
	public MessageResponse say(String userId, String text, HttpSession wasSession)
	{
		UserSession session = null;
		
		Object usr = wasSession.getAttribute(userId);
		if ( usr == null ) {
    		try {
    			session = fetchUserInfo(userId);
    			wasSession.setAttribute(userId, session);
    		}catch(Exception e) {}
		}
		else {
			session = (UserSession)usr;
		}
		
		if ( text == null ) text = "";
		
		if ( "__clear__".equals(text) ) {
			session.context = new Context();
			session.context.put("timezone", "Asia/Seoul");
			session.context.put("userInfo", session.userInfo);
			text = "";
    		try {
    			session = fetchUserInfo(userId);
    			wasSession.setAttribute(userId, session);
    		}catch(Exception e) {}
		}
		
		/*
		 * 사용자 정보 설정
		 */
		session.context.put("userInfo", session.userInfo.toMap());
		
		MessageInput input = new MessageInput();
		input.setText(text);
		
		MessageOptions options = new MessageOptions.Builder(workspaceId)
				.input(input)
				.context(session.context)
				.build();
		
		MessageResponse response = service.message(options).execute().getResult();
		
		session.context = response.getContext();
		
		Object o = response.getOutput().get("ACTION");
		
		if ( o != null ) {
			
			Map map = (Map)o;
			
			if (map.get("agree") != null && map.get("agree").toString().equals("Y")) { // 약관동의
				session.userInfo.setAgree01("Y");
				session.userInfo.setAgree02("Y");
				session.userInfo.setAgree03("Y");
				session.userInfo.setAgree04("Y");
				session.userInfo.setAgree05("Y");
			}
			
			if (map.get("term") != null) { // 가입기간
				session.userInfo.setTerm(Integer.parseInt(map.get("term").toString()));
			}
			
			if (map.get("cycl") != null) { // 납입주기 
				session.userInfo.setCycl(map.get("cycl").toString());
			}
			
			if (map.get("amt") != null) { // 금액
				session.userInfo.setAmt(Integer.parseInt(map.get("amt").toString()));
			}
			
			if (map.get("trsfDate") != null) { // 이체지정일
				session.userInfo.setTrsfDate(Integer.parseInt(map.get("trsfDate").toString()));
			}
			
			if (map.get("autoTrsfYn") != null) { // 자동이체여부 
				session.userInfo.setAutoTrsfYn(map.get("autoTrsfYn").toString());
			}
			
			if (map.get("expiCnclDiv") != null) { // 만기해지구분 
				session.userInfo.setExpiCnclDiv(map.get("expiCnclDiv").toString());
			}
			
			if (map.get("smsYn") != null) { // SMS여부
				session.userInfo.setSmsYn(map.get("smsYn").toString());
			}
			
			if (map.get("payAcctNo") != null && map.get("payAcctNo").toString().equals("Y")) { // 출금계좌번호
				System.out.println("payAcctNo : " + session.userInfo.getAcctNo());
				session.userInfo.setPayAcctNo(session.userInfo.getAcctNo());
			}
			
			//System.out.println("test---------------------->>> : ");
			
			if (map.get("stepId") != null && map.get("stepId").toString().equals("0401")) { // STEPID가 0401 가입완료일 경우
				
				session.userInfo.setStepId("0401");
				
				//System.out.println("test222---------------------->>> : " +session.userInfo.getUserId().substring(0,4));
				
				if (session.userInfo.getUserId() != null && session.userInfo.getUserId().substring(0,4).equals("test")) { // test ID인경우만
				
					//System.out.println("userId : " + session.userInfo.getUserId());
					
					//final String uri = "http://localhost:10000/event";
					
					final String uri = "http://pengsoo-service.169.56.170.165.nip.io/event";
					//RestTemplate restTemplate = new RestTemplate();
					//Object result = restTemplate.postForObject(uri, session.userInfo, Object.class);
				
					RestTemplate rest = new RestTemplate();
					
					ResponseEntity<String> result = rest.postForEntity(uri, session.userInfo, String.class); 
				
					System.out.println("Code : " + result.getStatusCode());

				}
				
				/*session.userInfo.setStepId(null);
				session.userInfo.setAgree01(null);
				session.userInfo.setAgree02(null);
				session.userInfo.setAgree03(null);
				session.userInfo.setAgree04(null);
				session.userInfo.setAgree05(null);
				
				session.userInfo.setCycl(null);         //납입주기
				session.userInfo.setTerm(null);         //가입기간
				session.userInfo.setAmt(null);          //금액
				session.userInfo.setTrsfDate(null);     //이체지정일
				session.userInfo.setAutoTrsfYn(null);   //자동이체여부
				session.userInfo.setExpiCnclDiv(null);  //만기해지구분
				session.userInfo.setSmsYn(null);        //SMS여부
				session.userInfo.setPayAcctNo(null);	//출금계좌번호
				*/			
				//session.userInfo = null;
				
	
				
				
			}
			
			
		}
			
			if ( o != null ) {
				String userData = o.toString();
				System.err.println("@.@ USER DATA = " + userData);
			}
			
			System.out.println(response); 
			

		return response;
	}

}
