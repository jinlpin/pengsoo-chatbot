package com.klab;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.ibm.watson.assistant.v1.model.MessageResponse;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@RestController
@Api(value="Chatbot API")
public class ChatbotController
{
	@Autowired
	WAService waService;

	@ApiOperation(value="챗봇 대화")
	@RequestMapping(value = "/say", method = RequestMethod.POST)
	public ResponseEntity<ResponsePayload> say(@RequestBody RequestPayload payload, HttpSession session)
	{
		MessageResponse res = waService.say(payload.getUserId(), payload.getText(), session);
		
		ResponsePayload out = new ResponsePayload();
		
		String txt = String.join("<br>", res.getOutput().getText());
		txt = txt.replaceAll("\\n", "<br>");
		
		out.setResponseText(txt);
		
		return new ResponseEntity<ResponsePayload>(out, HttpStatus.OK);
	}
}
