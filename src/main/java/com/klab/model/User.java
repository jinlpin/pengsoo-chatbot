package com.klab.model;

import java.util.Map;

import com.fasterxml.jackson.databind.ObjectMapper;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class User {
	/*
	 * private String usetId; private String userName; private String stepId;
	 * private String accountId; private String productId;
	 */
	private String userId;
	private String userNm;
	private String passWd;
	private String psnlInfoCollYn;
	private String mktYn;
	private String mktUseYn;
	private String acctNo;
	private String prdtId;
	private String stepId;
	private String stepName;
	private String agree01;
	private String agree02;
	private String agree03;
	private String agree04;
	private String agree05;
	private Integer term;
	private String cycl;
	private Integer trsfDate;
	private Integer amt;
	private String payAcctNo;
	private String expiCnclDiv; 
	private String autoTrsfYn; 
	private String smsYn;
	
	
	@SuppressWarnings("rawtypes")
	public Map toMap() {
		ObjectMapper oMapper = new ObjectMapper();
		return oMapper.convertValue(this, Map.class);
	}
}
