package com.klab.dao;

import org.apache.ibatis.annotations.Mapper;

import com.klab.model.User;

@Mapper
public interface UserDao {
	/**
	 * 사용자  정보 가져오기 
	 * @return
	 * @throws Exception
	 */
	User selectUser(String userId) throws Exception;
	
	int insertUserEvent(User userEvent) throws Exception;
}
