package com.klab;

import java.util.Map;

@SuppressWarnings("rawtypes")
public class ResponsePayload
{
	private String responseText;
	private Map userContext;

	public String getResponseText() {
		return responseText;
	}
	public void setResponseText(String responseText) {
		this.responseText = responseText;
	}
	public Map getUserContext() {
		return userContext;
	}
	public void setUserContext(Map userContext) {
		this.userContext = userContext;
	}
}
